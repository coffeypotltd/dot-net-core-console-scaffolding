﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotNetCoreConsoleAppScaffolding.Logic
{
    public interface IConsoleService
    {
        void DoWork();
    }
}
