﻿using dotNetCoreConsoleAppScaffolding.Models.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace dotNetCoreConsoleAppScaffolding.Logic
{
    public class ConsoleService : IConsoleService
    {
        private readonly IOptions<ClientConfiguration> _config;
        private readonly ILogger<ConsoleService> _logger;
        
        public ConsoleService(IOptions<ClientConfiguration> config, ILogger<ConsoleService> logger)
        {
            _config = config;
            _logger = logger;
            _logger.LogInformation("Test Console Service Constructor");
        }

        public void DoWork()
        {
            _logger.LogInformation("Working");
            return;
        }
    }
}
