﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotNetCoreConsoleAppScaffolding.Models.Configuration
{
    public class ClientConfiguration
    {
        public Client[] Clients { get; set; }
    }
}
