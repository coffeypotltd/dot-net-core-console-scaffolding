﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotNetCoreConsoleAppScaffolding.Models.Configuration
{
    public class Client
    {
        public string ClientName { get; set; }
        public string ClientID { get; set; }
    }
}
