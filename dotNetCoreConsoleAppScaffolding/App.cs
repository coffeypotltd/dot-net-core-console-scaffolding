﻿using dotNetCoreConsoleAppScaffolding.Logic;
using dotNetCoreConsoleAppScaffolding.Models.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace dotNetCoreConsoleAppScaffolding
{
    public class App
    {
        private readonly IConsoleService _console;
        private readonly ILogger<App> _logger;
        private readonly IOptions<ClientConfiguration> _config;

        public App(IConsoleService console, ILogger<App> logger, IOptions<ClientConfiguration> config)
        {
            _console = console;
            _logger = logger;
            _config = config;
        }

        public void Run()
        {
            foreach(var client in _config.Value.Clients)
            {
                _logger.LogInformation($"This is a console application for {client.ClientName}");
            }
            _console.DoWork();
        }
    }
}
