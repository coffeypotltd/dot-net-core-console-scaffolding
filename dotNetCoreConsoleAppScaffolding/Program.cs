﻿using dotNetCoreConsoleAppScaffolding.Logic;
using dotNetCoreConsoleAppScaffolding.Models.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.IO;

namespace dotNetCoreConsoleAppScaffolding
{
    class Program
    {
        static void Main(string[] args)
        {            
            ServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            ServiceProvider serviceProvider = services.BuildServiceProvider();
			ILogger<Program> logger = serviceProvider.GetService<ILoggerFactory>().AddLog4Net().CreateLogger<Program>();
            serviceProvider.GetService<App>().Run();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile("MultipleConfig.json", optional: true, reloadOnChange: true)
                .Build();

            services.AddLogging(x =>
                x.SetMinimumLevel(LogLevel.Debug)
                .AddConsole()
            );

            services.AddOptions();
            services.Configure<ClientConfiguration>(configuration.GetSection("ConfiguredClients"));
            services.AddTransient<IConsoleService, ConsoleService>();
            services.AddTransient<App>();
        }
    }
}
