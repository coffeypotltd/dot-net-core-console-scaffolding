﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Text;
using dotNetCoreConsoleAppScaffolding.Logic;
using dotNetCoreConsoleAppScaffolding.Models.Configuration;
using Microsoft.Extensions.Logging;
using System.IO;

namespace dotNetCoreconsoleAppScaffolding.Tests
{
    public class Unity
    {

        public void ConfigureServices(IServiceCollection services)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile("MultipleConfig.json", optional: true, reloadOnChange: true)
                .Build();

            services.AddLogging(x =>
                x.SetMinimumLevel(LogLevel.Debug)
                .AddConsole()
            );

            services.AddOptions();
            services.Configure<ClientConfiguration>(configuration.GetSection("ConfiguredClients"));
            services.AddTransient<IConsoleService, ConsoleService>();
        }
    }
}
