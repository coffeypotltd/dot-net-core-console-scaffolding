﻿using dotNetCoreConsoleAppScaffolding.Logic;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;

namespace dotNetCoreconsoleAppScaffolding.Tests
{
    [TestFixture]
    public class ConsoleTests
    {
        private ServiceProvider _serviceProvider;
        private Unity _unity;

        public ConsoleTests()
        {
            _unity = new Unity();
            ServiceCollection services = new ServiceCollection();
            _unity.ConfigureServices(services);
            _serviceProvider = services.BuildServiceProvider();
        }

        [TestCase]
        public void DoWorkTest()
        {
            try
            {
                var console = _serviceProvider.GetService<IConsoleService>();
                console.DoWork();
            }
            catch
            {
                Assert.Fail();
            }
        }
    }
}
